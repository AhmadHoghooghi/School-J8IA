package spittr.web;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class HomeControllerTestPOJO {
    @Test
    public void testHomePage() throws Exception{
        HomeController homeController = new HomeController();
        assertThat("home", is(equalTo(homeController.home())));
    }
}