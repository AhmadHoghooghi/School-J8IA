package explicitConfig.java;

import applicationDomainObjects.CDPlayer;
import applicationDomainObjects.CompactDisc;
import applicationDomainObjects.Gharibeh;
import applicationDomainObjects.MediaPlayer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExplicitAppConfig {
    @Bean
    public MediaPlayer getMediaPlayer(CompactDisc cd){
        return new CDPlayer(cd);
    }

    @Bean
    public CompactDisc getCompactDisk(){
        return new Gharibeh();
    }
}
