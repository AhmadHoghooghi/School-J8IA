package mix.xmlConfig.toJavaConfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations = {"spring/mediaPlayerConfig.xml"})
public class MixingXMLToJavaAppConfig {
}
