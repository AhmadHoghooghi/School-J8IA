package mix.javaConfig.toJavaConfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(explicitConfig.java.ExplicitAppConfig.class)
public class MixingJavaToJavaAppConfig {
}
