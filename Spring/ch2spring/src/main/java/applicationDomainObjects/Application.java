package applicationDomainObjects;

import autoConfig.java.AutoAppConfig;
import explicitConfig.java.ExplicitAppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) {
        // autoWiringActivateComponentScanViaJava();
        // autoWiringActivateComponentScanViaXML();
        // explicitJavaConfig();
        // explicitXmlConfig();
        // mixJavaConfigToJavaConfig();
        // mixXMLConfigToJavaConfig();
         mixJavaConfigToXMLConfig();
        //mixXMLConfigToXMLConfig();
    }

    private static void autoWiringActivateComponentScanViaXML() {
        //java/main/resources is standard place to put configuration files and it should not be mentioned
        // in file path, otherwise file will not be found.
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring/autoWiringActivateComponentScanViaXML.xml");
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    private static void autoWiringActivateComponentScanViaJava() {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(AutoAppConfig.class);
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    private static void explicitJavaConfig() {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(ExplicitAppConfig.class);
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    private static void explicitXmlConfig(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring/mediaPlayerConfig.xml");
        //note that just concrete types are mentioned in xml file but you can reterive object here via its interface
        //and concrete type.
        // both of these lines work.
        //MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        MediaPlayer mediaPlayer = context.getBean(CDPlayer.class);
        mediaPlayer.playDisk();
    }

    private static void mixJavaConfigToJavaConfig() {
        ApplicationContext context = new AnnotationConfigApplicationContext(mix.javaConfig.toJavaConfig.MixingJavaToJavaAppConfig.class);
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    private static void mixXMLConfigToJavaConfig() {
        ApplicationContext context = new AnnotationConfigApplicationContext(mix.xmlConfig.toJavaConfig.MixingXMLToJavaAppConfig.class);
        MediaPlayer mediaPlayer = context.getBean(CDPlayer.class);
        mediaPlayer.playDisk();
    }

    /**
     * note line <context:annotation-config /> in xml file which enables java annotation check
     */
    private static void mixJavaConfigToXMLConfig() {

        ApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:spring/importingJavaConfig.xml");
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }

    private static void mixXMLConfigToXMLConfig() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("classpath:spring/importingXMLConfig.xml");
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        mediaPlayer.playDisk();
    }
}
