package applicationDomainObjects;

import org.springframework.stereotype.Component;

@Component
public class Gharibeh implements CompactDisc {
    private String title="Gharibeh";
    private String artist = "Fereydoun Asorayi";

    @Override
    public void play() {
        System.out.println("playing " + title + " from " + artist);
    }
}
