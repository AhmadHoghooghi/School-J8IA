package dependencyInjection;

import applicationDomainObjects.MediaPlayer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/importingJavaConfig.xml")
public class MixJavaConfigToXMLConfig {

    @Autowired
    private ApplicationContext context;
    @Test
    public void testMediaPlayerIsNotNullWhileImportingJavaConfigToXML(){
        MediaPlayer mediaPlayer = context.getBean(MediaPlayer.class);
        assertThat(mediaPlayer, is(notNullValue()));
    }
}
