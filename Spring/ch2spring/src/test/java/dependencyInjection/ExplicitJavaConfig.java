package dependencyInjection;

import applicationDomainObjects.CompactDisc;
import explicitConfig.java.ExplicitAppConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ExplicitAppConfig.class)
public class ExplicitJavaConfig {
    @Autowired
    private CompactDisc cd;

    @Test
    public void cdIsNotNullAfterWiringViaJavaConfig(){
        assertThat(cd, is(notNullValue()));
    }
}
