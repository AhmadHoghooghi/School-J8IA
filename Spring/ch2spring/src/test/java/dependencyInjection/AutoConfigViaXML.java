package dependencyInjection;

import applicationDomainObjects.CompactDisc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
//in tests classpath should be included
@ContextConfiguration(locations = {"classpath:spring/autoWiringActivateComponentScanViaXML.xml"})
public class AutoConfigViaXML {
    @Autowired
    private CompactDisc cd;
    @Test
    public void testCompactDiskIsNotNullViaXMLAutoScan(){
        assertThat(cd, is(notNullValue()));
    }


}
