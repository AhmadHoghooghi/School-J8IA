package dependencyInjection;

import applicationDomainObjects.MediaPlayer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/importingXMLConfig.xml"})
public class MixXMLConfigToXMLConfig {
    @Autowired
    private MediaPlayer mp;

    @Test
    public void testMediaPlayerIsNotNullWhileImportingXMLConfigToXMLConfig(){
        assertThat(mp, is(notNullValue()));
        mp.playDisk();
    }
}

