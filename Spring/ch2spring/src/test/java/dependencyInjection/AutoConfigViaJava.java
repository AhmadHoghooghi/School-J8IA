package dependencyInjection;

import applicationDomainObjects.CompactDisc;
import autoConfig.java.AutoAppConfig;
import explicitConfig.java.ExplicitAppConfig;
import explicitConfig.java.ExplicitAppConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AutoAppConfig.class)
public class AutoConfigViaJava {
    @Autowired
    private CompactDisc cd;

    @Test
    public void cdIsNotNullAfterAutoWiringViaJava() {
        assertThat(cd, is(notNullValue()));
        System.out.println();
    }
}
