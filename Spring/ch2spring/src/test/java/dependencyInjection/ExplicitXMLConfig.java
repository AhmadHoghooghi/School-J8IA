package dependencyInjection;

import applicationDomainObjects.CompactDisc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
//in tests classpath should be included
@ContextConfiguration(locations = {"classpath:spring/mediaPlayerConfig.xml"})
public class ExplicitXMLConfig {

    @Autowired
    ApplicationContext context;

    @Autowired
    private CompactDisc cd;

    @Test
    public void testCompactDiskIsNotNullViaXMLConfiguration() {
        assertThat(cd, is(notNullValue()));
        //cd.playDisk();
    }

    @Test
    public void testCompactDiscRetrievalViaApplicationContext() {
        CompactDisc cd = context.getBean(CompactDisc.class);
        assertThat(cd, is(notNullValue()));
        // cd.playDisk();
    }
}
