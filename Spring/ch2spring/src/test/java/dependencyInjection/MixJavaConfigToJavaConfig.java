package dependencyInjection;

import applicationDomainObjects.MediaPlayer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = mix.javaConfig.toJavaConfig.MixingJavaToJavaAppConfig.class)
public class MixJavaConfigToJavaConfig {
    @Autowired
    private ApplicationContext context;

    @Test
    public void mediaPlayerIsNotNullWhileMixingJavaConfigToJavaConfig(){
        MediaPlayer cd = context.getBean(MediaPlayer.class);
        assertThat(cd, is(notNullValue()));
    }
}
