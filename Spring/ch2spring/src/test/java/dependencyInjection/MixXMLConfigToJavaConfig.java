package dependencyInjection;

import applicationDomainObjects.MediaPlayer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {mix.xmlConfig.toJavaConfig.MixingXMLToJavaAppConfig.class})
public class MixXMLConfigToJavaConfig {
    @Autowired
    private MediaPlayer mp;

    @Test
    public void testMediaPlayerIsNotNullWhileMixingXmlConfigToJavaConfig(){
        assertThat(mp, is(notNullValue()));
        mp.playDisk();
    }
}
