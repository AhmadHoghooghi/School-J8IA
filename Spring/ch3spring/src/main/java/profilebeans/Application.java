package profilebeans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    static ApplicationContext context;
    static{
        context = new AnnotationConfigApplicationContext(ProfileBeanConfig.class);
    }
    public static void main(String[] args) {
        ValuedBean valuedBean = context.getBean(ValuedBean.class);
        System.out.println(valuedBean.getValue());
    }
}
