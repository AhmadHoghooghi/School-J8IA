package profilebeans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource({"classpath:application.properties"})
public class ProfileBeanConfig {

    // @Bean
    // public ValuedBean getBeanWhenNoProfileIsActive(){
    //     return new ValuedBean("NoProfile");
    // }
    //
    // @Bean
    // public PropertySourcesPlaceholderConfigurer placePlaceHolderConfigurer() {
    //     return new PropertySourcesPlaceholderConfigurer();
    // }
    //
   @Bean
   @Profile("profile1")
   public ValuedBean getBeanWhenProfile1IsActive(){
       return new ValuedBean("profile1");
   }

    @Bean
    @Profile("profile2")
    public ValuedBean getBeanWhenProfile2IsActive(){
        return new ValuedBean("profile2");
    }
}
