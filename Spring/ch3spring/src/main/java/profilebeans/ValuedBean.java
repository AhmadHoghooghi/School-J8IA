package profilebeans;

public class ValuedBean {
    private String value;

    public ValuedBean(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "ValuedBean{" +
                "value='" + value + '\'' +
                '}';
    }
}

