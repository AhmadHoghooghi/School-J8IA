package beanproxies;

import beanproxies.domain.Camera;
import beanproxies.domain.FlashNegative;
import beanproxies.domain.Negative;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Configuration
public class CameraAppConfig {

    @Bean
    //proxyMode = ScopedProxyMode.INTERFACES
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Negative getNegative(){
        return new FlashNegative();
    }

    @Bean
    public Camera getCamera(){
        return new Camera();
    }
}
