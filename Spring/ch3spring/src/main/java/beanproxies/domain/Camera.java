package beanproxies.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class Camera {
    //test get bean
    @Autowired
    private ApplicationContext context;


    public Camera() {
    }


    /**
     * every photo should be taken on new instance of negative and should have different serial number;
     */
    public void takeAPhotoOf(String photoContent) {
        Negative rawNegative = context.getBean(Negative.class);
        System.out.println("photo of "+photoContent+"is taken on with serialNumber: "+rawNegative.getSerialNumber());
        rawNegative.getSerialNumber();
    }

   /* public void takeAPhotoOf(String photoContent) {
        loadRawNegative();
        saveOnRawNegative(photoContent);
        saveUsedNegativeInListOfTakenPhotos();
    */
}
