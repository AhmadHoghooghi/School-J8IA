package beanproxies.domain;

public interface Negative {

    long getSerialNumber();
}
