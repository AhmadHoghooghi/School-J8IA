package beanproxies.domain;

public class FlashNegative implements Negative {
    private static long serialNumberCounter = 1000;
    private long serialNumber;
    private String content;

    public FlashNegative() {
        serialNumber = serialNumberCounter;
        serialNumberCounter++;

    }
    @Override
    public long getSerialNumber() {
        return serialNumber;
    }

    @Override
    public String toString() {
        return "FlashNegative{" +
                "serialNumber=" + serialNumber +
                '}';
    }
}
