package primarybean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    private static ApplicationContext context;
    static {
        context = new AnnotationConfigApplicationContext(MenuAppConfig.class);
    }

    public static void main(String[] args) {
        Menu menu = context.getBean(Menu.class);
        System.out.println(menu.getDessert().getClass());
    }
}
