package condition;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConditionalBeanConfig {

    @Bean
    @Conditional(BeanInstantiationCondition.class)
    public IntValueBean getIntValueBean1(){
        return  new IntValueBean(1);
    }
}
