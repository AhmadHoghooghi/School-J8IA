package condition;

public class IntValueBean {
    private int value;

    public IntValueBean(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "IntValueBean{" +
                "value=" + value +
                '}';
    }
}
