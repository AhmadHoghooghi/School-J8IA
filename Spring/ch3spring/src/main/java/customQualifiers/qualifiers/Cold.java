package customQualifiers.qualifiers;

import org.springframework.beans.factory.annotation.Qualifier;

import static java.lang.annotation.ElementType.*;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {CONSTRUCTOR, FIELD, TYPE, METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Cold {
}
