package customQualifiers;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"customQualifiers.domain"})
public class CustomQualifiersConfig {
}
