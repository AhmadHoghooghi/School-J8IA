package customQualifiers.domain;

import customQualifiers.qualifiers.Cold;
import customQualifiers.qualifiers.Fruity;
import org.springframework.stereotype.Component;

@Component
@Fruity
@Cold
public class Popsicle implements Dessert {
}
