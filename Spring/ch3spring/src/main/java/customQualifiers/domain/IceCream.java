package customQualifiers.domain;

import customQualifiers.qualifiers.Cold;
import customQualifiers.qualifiers.Creamy;
import org.springframework.stereotype.Component;

@Component
@Creamy
@Cold
public class IceCream implements Dessert {
}
