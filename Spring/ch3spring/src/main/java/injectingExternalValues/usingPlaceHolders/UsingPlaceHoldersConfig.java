package injectingExternalValues.usingPlaceHolders;

import injectingExternalValues.domain.CustomDisc;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource({"classpath:injectingExternalValues/album.properties"})
public class UsingPlaceHoldersConfig {

    @Bean
    public PropertySourcesPlaceholderConfigurer placePlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public CustomDisc getCustomDisk(@Value("${album.title}") String title,
                                    @Value("${album.artist}") String artist) {
        return new CustomDisc(title,artist);
    }
}
