package beanproxies;

import beanproxies.domain.Camera;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CameraAppConfig.class)
public class BeanProxiesTest {
    @Autowired
    private ApplicationContext context;

    @Test
    public void testCameraInjection()
    {
        Camera camera = context.getBean(Camera.class);
        assertThat(camera, is(notNullValue()));
    }

    @Test
    public void testNegativeInstantiation(){
        Camera camera = context.getBean(Camera.class);
        camera.takeAPhotoOf("mountains");
        camera.takeAPhotoOf("rivers");
        camera.takeAPhotoOf("animals");
    }
}
