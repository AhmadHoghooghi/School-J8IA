package primarybean;




import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MenuAppConfig.class)
public class ApplicationTest {
    @Autowired
    private Dessert dessert;

    @Test
    public void testPrimaryBeanInjectionOnCake(){
        assertThat(dessert, is(instanceOf(Cake.class)));
        // assertThat(dessert, isA(Cake.class));
    }

}