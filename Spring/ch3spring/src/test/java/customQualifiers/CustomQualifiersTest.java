package customQualifiers;

import customQualifiers.domain.Dessert;
import customQualifiers.domain.IceCream;
import customQualifiers.domain.Popsicle;
import customQualifiers.qualifiers.Cold;
import customQualifiers.qualifiers.Creamy;
import customQualifiers.qualifiers.Fruity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CustomQualifiersConfig.class)
public class CustomQualifiersTest {
    /**
     * note that is name of fields, dessert1 is changed to popsicle
     * and @Fruity is removed, auto wiring will solve ambiguity with field name.
     */
    @Autowired
    @Cold
    @Fruity
    private Dessert dessert1;

    @Autowired
    @Cold
    @Creamy
    private Dessert dessert2;

    @Test
    public void popsicleIsWiredProperly() {
        assertThat(dessert1, is(instanceOf(Popsicle.class)));
        System.out.println(dessert1.getClass());
    }

    @Test
    public void iceCreamIsWiredProperly() {
        assertThat(dessert2, is(instanceOf(IceCream.class)));
        System.out.println(dessert2.getClass());
    }


}
