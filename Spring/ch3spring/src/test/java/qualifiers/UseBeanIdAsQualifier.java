package qualifiers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import primarybean.Dessert;
import primarybean.IceCream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = primarybean.MenuAppConfig.class)
public class UseBeanIdAsQualifier {

    @Autowired
    @Qualifier("iceCream")
    private Dessert dessert;

    @Test
    public void testUsingQualifierWithBeanId(){
        assertThat(dessert, is(instanceOf(IceCream.class)));
    }

}
