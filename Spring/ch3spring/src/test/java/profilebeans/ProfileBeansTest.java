package profilebeans;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ProfileBeanConfig.class)
@ActiveProfiles("profile1")
public class ProfileBeansTest {

    @Autowired
    private ValuedBean valuedBean;
    @Test
    public void checkProfile1ProperBeanIsCreated(){
        assertThat("profile1 is active", valuedBean.getValue(), is(equalTo("profile1")));
        //System.out.println("valuedBean = " + valuedBean);

    }


}