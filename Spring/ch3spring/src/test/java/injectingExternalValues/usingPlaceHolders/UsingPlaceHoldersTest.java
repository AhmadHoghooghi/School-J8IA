package injectingExternalValues.usingPlaceHolders;

import injectingExternalValues.domain.CustomDisc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = UsingPlaceHoldersConfig.class)
public class UsingPlaceHoldersTest {
    @Autowired
    private CustomDisc disk;

    @Test
    public void testDiskInstantiation() {
        assertThat(disk, is(notNullValue()));
        assertThat(disk.getTitle(), is(equalTo("Gharibeh")));
        assertThat(disk.getArtist(), is(equalTo("Fereydon Asorayi")));
    }
}