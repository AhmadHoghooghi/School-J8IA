package ch1example;

import java.io.PrintStream;

public class SlayDragonQuest implements Quest {
    private PrintStream ps;

    public SlayDragonQuest(PrintStream ps) {
        this.ps = ps;
    }

    @Override
    public void embark() {
        ps.println("Embarking on quest to slay the dragon");
    }
}
