package ch1example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KnightConfig {

    @Bean
    public Knight getKnight(){
        return  new BraveKnight(getQuest());
    }
    @Bean
    public Quest getQuest() {
        return new SlayDragonQuest(System.out);
    }
}
