package hamcrest;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Every.everyItem;

public class ListMatchers {
    private static final List<Integer> intList = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));

    @Test
    public void testHasSize() {
        assertThat(intList, hasSize(6));
    }

    @Test
    public void testContains() {
        assertThat(intList, contains(1, 2, 3, 4, 5, 6));
    }

    @Test
    public void testContainsInAnyOrder() {
        assertThat(intList, containsInAnyOrder(1, 2, 3, 4, 6, 5));
    }

    @Test
    public void testEveryItem() {
        assertThat(intList, everyItem(greaterThan(0)));
    }

    @Test
    public void testEveryItemLessThan() {
        assertThat(intList, everyItem(lessThanOrEqualTo(6)));

    }
}
