package hamcrest;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class Iterables {
    private static List<Integer> intList;

    @BeforeClass
    public static void setUpList() {
        intList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    }
    @Test
    public void testEveryItem(){
        assertThat(intList, everyItem(is(lessThanOrEqualTo(10))));
    }

    @Test
    public void testHasItemT(){
        //hasItem(T)
        assertThat(intList, hasItem(10));
    }
    @Test
    public void testHasItemMatcher1(){
        //hasItem(Matcher<T> matcher)
        assertThat(intList, hasItem(equalTo(10)));
    }
    @Test
    public void testHasItemMatcher2(){
        //hasItem(Matcher<T> matcher)
        assertThat(intList, hasItem(greaterThan(5)));
    }

    @Test
    public void testHasItemsT(){
        assertThat(intList, hasItems(2,3));
    }

    @Test
    public void testHasItemsMatcher() {
        assertThat(intList, hasItems(greaterThan(8), lessThanOrEqualTo(2)));
    }

    @Test
    public void testEmptyIterable(){
        List<Integer> emptyList =Collections.emptyList();
        assertThat(emptyList, is(emptyIterable()));
    }
}
