package hamcrest;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.MatcherAssert.assertThat;

public class IsAMatcher {
    @Test
    public void testIsA(){
        assertThat(Long.valueOf(1), isA(Long.class));
    }

}
