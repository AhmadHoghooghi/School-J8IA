package hamcrest;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class StringMatchers {
    @Test
    public void testIsEmptyString(){
        String s = "";
        assertThat(s, isEmptyString());
    }

    @Test
    public void testIsEmptyOrNullString() {
        String s = null;
        assertThat(s, isEmptyOrNullString());
    }

    @Test
    public void testContainsString(){
        String s = "ahmad is happy";
        assertThat(s, containsString("ahmad"));
        assertThat(s, containsString("is"));
    }

    @Test
    public void testStartsWith() {
        String s = "ahmad is happy";
        assertThat(s, startsWith("ahmad"));
    }
    @Test
    public void testEndsWith() {
        String s = "ahmad is happy";
        assertThat(s, endsWith("happy"));
    }

    @Test
    public void testEqualToIgnoreCase() {
        String s = "ahmad is happy";
        assertThat(s, equalToIgnoringCase("AHMAD IS HAPPY"));
    }

    /**
     * Spaces in head and tail are ignored, spaces between words is collapsed to
     * one space.
     */
    @Test
    public void testEqualToIgnoringWhiteSpace() {
        String s = "ahmad    is happy\t";
        assertThat(s, equalToIgnoringWhiteSpace("  ahmad is    happy  "));
    }

    @Test
    public void TestStringContainsInOrder() {
        String s = "ahmad is happy";
        //order is important
        List<String> stringList = Arrays.asList("is","happy");
        assertThat(s, stringContainsInOrder(stringList));
    }
}
