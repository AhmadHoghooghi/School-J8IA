package domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EnhancedSequenceIdMessage {

    @Id
    @GeneratedValue(generator = "ID_GENERATOR")
    private Long id;

    private String content;

    public EnhancedSequenceIdMessage(){

    }

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
