import EMUtil.PersistenceUtil;
import domain.*;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class TestIdGenerationStrategies {

    @Test
    public void testAutoIdGenerationStrategy() {
        AutoIdMessage message = new AutoIdMessage();
        message.setContent("Auto Id message");
        Long id = PersistenceUtil.runJpaCode(em -> {
            em.persist(message);
            return message.getId();
        });
        assertThat(id, is(notNullValue()));
    }

    @Test
    public void testSequenceIdGenerationStrategy() {
        SequenceIdMessage message = new SequenceIdMessage();
        message.setContent("Sequence Id message");
        Long id = PersistenceUtil.runJpaCode(em -> {
            em.persist(message);
            return message.getId();
        });
        assertThat(id, is(notNullValue()));
    }

    @Test
    public void testIdentityIdGenerationStrategy() {
        IdentityIdMessage message = new IdentityIdMessage();
        message.setContent("Identity Id message");
        Long id = PersistenceUtil.runJpaCode(em -> {
            em.persist(message);
            return message.getId();
        });
        assertThat(id, is(notNullValue()));
    }

    @Test
    public void testTableIdGenerationStrategy() {
        TableIdMessage message = new TableIdMessage();
        message.setContent("table Id message");
        Long id = PersistenceUtil.runJpaCode(em -> {
            em.persist(message);
            return message.getId();
        });
        assertThat(id, is(notNullValue()));
    }

    @Test
    public void testTableEnhancedSequenceStrategy() {
        EnhancedSequenceIdMessage message = new EnhancedSequenceIdMessage();
        message.setContent("Enhanced Sequence Id message");
        Long id = PersistenceUtil.runJpaCode(em -> {
            em.persist(message);
            return message.getId();
        });
        assertThat(id, is(notNullValue()));
    }
}
