to Run Project

* Clone application 
* Put `h2-1.4.197.jar` file in `db` folder (project root) 
* Run `runInServerMode.bat` file to start h2 in server mode
* Run project tests.
* Enjoy! 