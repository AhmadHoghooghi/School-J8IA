package data;

import domain.Message;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PersistenceTest {

    @Test
    public void step1testMessagePersistence(){
        Message message = new Message();
        message.setContent("Hello World");
        Long id = PersistenceUtil.runJpaCode(em -> {
            em.persist(message);
            return message.getId();
        });
        System.out.println("id = " + id);


        /*EntityManager em = PersistenceUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(message);
        tx.commit();
        em.close();
        assertThat(message.getId(), is(notNullValue()));*/
    }

    @Test
    public void step2testMessageRead(){
        EntityManager em = PersistenceUtil.getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        List<Message> messageList = em.createQuery("select m from Message m", Message.class).getResultList();
        assertThat(messageList.size(), is(equalTo(1)));
        assertThat(messageList.get(0).getContent(), is(equalTo("Hello World")));
        tx.commit();
        em.close();
    }


}
