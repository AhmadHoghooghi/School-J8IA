import domainEmbeddableComponents.Message;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class MessageIOTest {
    @Test
    public void testMessagePersistence(){
        Message message = new Message();
        message.setContent("hi persistence");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("NewPersistenceUnit");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        em.persist(message);
        assertThat(message.getId(), is(Matchers.notNullValue()));

        tx.commit();
        em.close();
    }
}
