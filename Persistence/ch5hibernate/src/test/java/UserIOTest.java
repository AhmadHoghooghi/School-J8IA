import data.PersistenceUtil;
import domainEmbeddableComponents.Address;
import domainEmbeddableComponents.City;
import domainEmbeddableComponents.User;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class UserIOTest {

    @Test
    public void testAddressSave(){
        City tehran = new City("tehran", "iran");
        City paris = new City("paris", "france");

        Address homeAddress = new Address("enghelab", "12345", tehran);
        Address workAddress = new Address("21street", "54321", paris);

        User user = new User("ahmadUserName", "ahmad", "hoghooghi", homeAddress);
        user.setWorkAddress(workAddress);

        Long id = PersistenceUtil.runJpaCode(em -> {
            em.persist(user);
            return user.getId();
        });

        assertThat( id, is(notNullValue()));
    }


}
