import data.PersistenceUtil;
import domainJPAConverters.Item;
import domainJPAConverters.MonetaryAmount;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class TestJPAConverter {
    @Test
    public void testConverterPersist(){
        Item item = new Item("apple", new MonetaryAmount(new BigDecimal(3.25), Currency.getInstance("USD")));
        Long id = PersistenceUtil.runJpaCode(em -> {
            em.persist(item);
            return item.getId();
        });
        assertThat(id, is(notNullValue()));
    }
}
