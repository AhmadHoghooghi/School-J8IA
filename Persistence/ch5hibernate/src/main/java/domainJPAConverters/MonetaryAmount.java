package domainJPAConverters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;

public class MonetaryAmount implements Serializable {
    private BigDecimal price;
    private Currency currency;

    public MonetaryAmount(BigDecimal price, Currency currency) {
        this.price = price;
        this.currency = currency;
    }

    public BigDecimal getPrice() {
        return price;
    }


    public Currency getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MonetaryAmount that = (MonetaryAmount) o;
        return Objects.equals(price, that.price) &&
                Objects.equals(currency, that.currency);
    }

    @Override
    public int hashCode() {

        return Objects.hash(price, currency);
    }

    @Override
    public String toString() {
        return getPrice()+" "+getCurrency();
    }

    public static MonetaryAmount fromString(String s){
        String[] parts = s.split(" ");
        return new MonetaryAmount(new BigDecimal(parts[0]),Currency.getInstance(parts[1]));
    }
}
