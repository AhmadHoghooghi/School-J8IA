/**
 * example about mapping embeddable components.
 * each User has two address objects embedded in it and each address has an embedded city object.
 */
package domainEmbeddableComponents;