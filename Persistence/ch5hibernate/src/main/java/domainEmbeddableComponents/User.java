package domainEmbeddableComponents;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    private String userName;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "HOME_STREET", nullable = false)),
            @AttributeOverride(name = "zipCode", column = @Column(name = "HOME_ZIPCODE", nullable = false)),
            @AttributeOverride(name = "city.name", column = @Column(name = "HOME_CITY_NAME", nullable = false)),
            @AttributeOverride(name = "city.country", column = @Column(name = "HOME_CITY_COUNTRY", nullable = false))
    }
    )
    private Address homeAddress;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "WORK_STREET")),
            @AttributeOverride(name = "zipCode", column = @Column(name = "WORK_ZIPCODE")),
            @AttributeOverride(name = "city.name", column = @Column(name = "WORK_NAME")),
            @AttributeOverride(name = "city.country", column = @Column(name = "WORK_CITY_COUNTRY", nullable = false))
    }
    )
    private Address workAddress;

    protected User() {
    }

    public User(String userName, String firstName, String lastName, Address address) {
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.homeAddress = address;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Address getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(Address workAddress) {
        this.workAddress = workAddress;
    }
}
