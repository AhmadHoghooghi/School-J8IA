package application;

import data.PersistenceUtil;
import domainEmbeddableComponents.Address;
import domainEmbeddableComponents.City;
import domainEmbeddableComponents.Message;
import domainEmbeddableComponents.User;

public class Application {
    public static void main(String[] args) {
        //persistUser();

        Message message = new Message();
        message.setContent("Hi");
        Long generatedId = PersistenceUtil.runJpaCode(em -> {
            em.persist(message);
            long id = message.getId();
            return id;
        });
        System.out.println("generatedId = " + generatedId);
    }

    private static void persistUser() {
        City tehran = new City("tehran", "iran");
        City paris = new City("paris", "france");

        Address homeAddress = new Address("enghelab", "12345", tehran);
        Address workAddress = new Address("21street", "54321", paris);

        User user = new User("ahmadUserName", "ahmad", "hoghooghi", homeAddress);
        user.setWorkAddress(workAddress);

        Long id = PersistenceUtil.runJpaCode(em -> {
            em.persist(user);
            return user.getId();
        });
        System.out.println("id = " + id);
    }
}
