package com.dotin.fruitInstantiation;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

import java.util.function.Function;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;


public class FruitInstantiationTest {
    static Map<String, Function<Integer,Fruit>> name2ConstructorMap;
    static {
        name2ConstructorMap = new HashMap<>();
        name2ConstructorMap.put("orange", Orange::new);
        name2ConstructorMap.put("peach", Peach::new);
    }

    @Test
    public void test10gOrange(){
        Orange orange = (Orange)name2ConstructorMap.get("orange").apply(10);
        //System.out.println(orange);
        assertThat(orange, isA(Orange.class));

    }

}
