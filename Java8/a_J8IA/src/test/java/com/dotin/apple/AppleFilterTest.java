package com.dotin.apple;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.equalTo;

public class AppleFilterTest {
    private static List<Apple> appleList;

    @BeforeClass
    public static void setupAppleList() {
        appleList = new ArrayList<Apple>();
        appleList.add(new Apple("red", 10));
        appleList.add(new Apple("red", 160));
        appleList.add(new Apple("blue", 20));
        appleList.add(new Apple("blue", 170));
        appleList.add(new Apple("black", 30));
        appleList.add(new Apple("black", 180));
        appleList.add(new Apple("black", 190));
    }

    @Test
    public void testEmptyAppleList() {
        List<Apple> emptyAppleList = new ArrayList<>();
        ApplePredicate predicate = new WeightPredicate(150);
        List<Apple> resultList = AppleFilter.filter(emptyAppleList,predicate);
        assertThat(0, equalTo(resultList.size()));
    }

    @Test(expected = NullPointerException.class)
    public void testNullPredicate() {
        AppleFilter.filter(appleList,null);
    }

    @Test
    public void testWeightPredicate() {
        WeightPredicate wp = new WeightPredicate(150);
        List<Apple> resultList = AppleFilter.filter(appleList,wp);
        assertThat(4, equalTo(resultList.size()));
    }

    @Test
    public void testColorPredicate() throws IllegalAccessException {
        ColorPredicate cp = new ColorPredicate("black");
        List<Apple> resultList = AppleFilter.filter(appleList,cp);
        assertThat(3, equalTo(resultList.size()));
    }

    @Test
    public void testColorAndWeightPredicate() {
        ColorAndWeightPredicate cwp = new ColorAndWeightPredicate("black", 160);
        List<Apple> resultList = AppleFilter.filter(appleList,cwp);
        assertThat(2, equalTo(resultList.size()));
    }


}
