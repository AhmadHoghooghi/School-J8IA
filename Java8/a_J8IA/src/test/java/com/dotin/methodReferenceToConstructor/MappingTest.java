package com.dotin.methodReferenceToConstructor;


        import org.junit.Test;
        import com.dotin.apple.Apple;
        import static org.hamcrest.Matchers.is;
        import static org.hamcrest.Matchers.equalTo;
        import static org.hamcrest.Matchers.notNullValue;
        import static org.hamcrest.MatcherAssert.assertThat;

        import java.util.function.BiFunction;

/**
 * This class thest the way a AppleProperty can be mapped to Apple object
 * using method reference to its constructor.
 */
public class MappingTest {

    @Test
    public void testAppleMapping() {
        AppleProperty appleProperty = new AppleProperty("Blue", 150);
        BiFunction<String,Integer,Apple> appleCreator = Apple::new;
        Apple apple = appleCreator.apply(appleProperty.getColor(), appleProperty.getWeight());
        assertThat(apple, is(notNullValue()));
        assertThat(apple.getColor(), is(equalTo("Blue")));
    }
}
