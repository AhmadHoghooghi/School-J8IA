package com.dotin.abstractData;

import com.dotin.apple.Apple;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class AbstractFilterTest {
    private static List<Integer> intList;
    private static List<Apple> appleList;
    @BeforeClass
    public static void setUpLists(){
        Integer[] intArray = {1,2,3,4,5,6};
        intList = Arrays.asList(intArray);

        appleList = new ArrayList<Apple>();
        appleList.add(new Apple("red", 10));
        appleList.add(new Apple("red", 160));
        appleList.add(new Apple("blue", 20));
        appleList.add(new Apple("blue", 170));
        appleList.add(new Apple("black", 30));
        appleList.add(new Apple("black", 180));
        appleList.add(new Apple("black", 190));
    }

    @Test
    public void testIntPredicateWithAnonymousClass(){
        Predicate<Integer> intPred = new Predicate<Integer>() {
            @Override
            public boolean test(Integer num) {
                if (num>=4){
                    return true;
                }
                return false;
            }
        };

        List<Integer> resultList = AbstractFilter.filter(intList, intPred);
        MatcherAssert.assertThat(3, CoreMatchers.equalTo(resultList.size()));
    }

    @Test
    public void testApplePredicateWithLambda(){
        List<Apple> resultList = AbstractFilter.filter(appleList, apple -> apple.getWeight() >= 150);
        Assert.assertThat(4, CoreMatchers.equalTo(resultList.size()));
    }
}
