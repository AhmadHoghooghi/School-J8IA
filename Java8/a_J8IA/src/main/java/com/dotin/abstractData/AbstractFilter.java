package com.dotin.abstractData;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class AbstractFilter {

    public static <E> List<E> filter(List<E> list, Predicate<E> predicate) {
        List<E> resultList = new ArrayList<>();
        for (E e : list) {
            if (predicate.test(e)){
                resultList.add(e);
            }
        }
        return resultList;
    }
}
