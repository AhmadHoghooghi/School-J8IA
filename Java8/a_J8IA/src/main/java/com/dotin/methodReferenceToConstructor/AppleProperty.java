package com.dotin.methodReferenceToConstructor;

public class AppleProperty {
    private String Color;
    private int weight;

    public AppleProperty(String color, int weight) {
        if (color == null){
            throw new NullPointerException("color can not be NULL");
        }
        Color = color;
        this.weight = weight;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
