package com.dotin.fruitInstantiation;

public class Peach extends Fruit {
    public Peach(int weight) {
        super(weight);
    }

    @Override
    public String toString() {
        return "Peach{"+getWeight()+"gr}";
    }
}
