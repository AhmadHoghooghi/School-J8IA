package com.dotin.fruitInstantiation;

public class Orange extends Fruit {

    public Orange(int weight) {
        super(weight);
    }

    @Override
    public String toString() {
        return "Orange{"+getWeight()+"gr}";
    }
}
