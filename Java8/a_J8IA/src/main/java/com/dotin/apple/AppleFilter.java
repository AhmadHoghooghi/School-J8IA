package com.dotin.apple;

import java.util.ArrayList;
import java.util.List;

public class AppleFilter {
    public static List<Apple> filter(List<Apple> appleList, ApplePredicate predicate) {
        List<Apple> resultList = new ArrayList<>();
        for (Apple apple : appleList) {
            if (predicate.test(apple)) {
                resultList.add(apple);
            }
        }
        return resultList;
    }
}
