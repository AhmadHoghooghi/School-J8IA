package com.dotin.apple;

public class WeightPredicate implements ApplePredicate {
    private int identifierWeight;

    public WeightPredicate(int identifierWeight) {
        this.identifierWeight = identifierWeight;
    }

    @Override
    public boolean test(Apple apple) {
        return apple.getWeight()>=identifierWeight;
    }
}
