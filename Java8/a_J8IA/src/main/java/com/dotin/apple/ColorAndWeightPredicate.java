package com.dotin.apple;

public class ColorAndWeightPredicate implements ApplePredicate {
    private String color;
    private int weight;

    public ColorAndWeightPredicate(String color, int weight) {
        if (color == null) {
            throw new NullPointerException("color can not be null");
        }
        this.color = color;
        this.weight = weight;
    }

    @Override
    public boolean test(Apple apple) {
        return color.equals(apple.getColor()) && apple.getWeight()>=weight;
    }
}
