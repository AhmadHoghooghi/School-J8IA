package com.dotin.apple;

public class ColorPredicate implements ApplePredicate {
    private String color;

    public ColorPredicate(String color) throws IllegalAccessException {
        if (color==null){
            throw new IllegalAccessException("color can not Be null");
        }else {
            this.color = color;
        }
    }

    @Override
    public boolean test(Apple apple) {
        return color.equals(apple.getColor());
    }
}
