package com.dotin.apple;

public interface ApplePredicate {
    boolean test(Apple apple);
}
