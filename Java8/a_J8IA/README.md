**Review packages in order mentioned below:**
* `com.dotin.apple` : behavior parameterization, fully implemented
* `com.dotin.abstractData`: behavior parameterization, Using Java SE API utilities: `Predicate<E>`
* `com.dotin.methodReferenceToConstructor`: method reference to constructor of `Apple` class is created. Instantiation of Apple object is tested with mapping of `AppleProperty` object to `Apple`.
* `com.dotin.fruitInstantiation`: There is a map of fruit Name to its constructor. Instantiation of objects is done using method reference to constructor.
`name2ConstructorMap.get("orange").apply(10)` instantiates a 10gr `Orange` object.
 