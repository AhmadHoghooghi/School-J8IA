package shop.service;

import org.junit.Test;
import shop.service.ShopService;

import java.util.List;

public class ShopServiceTest {
    private static final int SHOP_NUM = 4;

    @Test
    public void testFindPricesSynchronous() {
        long start = System.nanoTime();
        List<String> result = ShopService.findPricesSynchronous("aPhone", SHOP_NUM);
        result.forEach(System.out::println);
        long end = System.nanoTime();
        System.out.println("Run took " + (end - start) / 1_000_000 + " m Sec");
    }

    @Test
    public void testFindWithParallelStream() {
        long start = System.nanoTime();
        List<String> result = ShopService.findPricesParallelStream("aPhone", SHOP_NUM);
        result.forEach(System.out::println);
        long end = System.nanoTime();
        System.out.println("Run took " + (end - start) / 1_000_000 + " m Sec");
    }

    /**
     * 3 shops: 1sec
     * 4 shops: 2sec
     * 6 shops: 2sec
     * 7 shops: 3sec
     * 9 shops: 3sec
     * It seams three are three free threads on common pool
     */
    @Test
    public void testFindWithAsynchronousDefaultExecutor() {
        long start = System.nanoTime();
        List<String> result = ShopService.findPricesASynchronousDefaultExecutor("aPhone", SHOP_NUM);
        result.forEach(System.out::println);
        long end = System.nanoTime();
        System.out.println("Run took " + (end - start) / 1_000_000 + " m Sec");
    }

    @Test
    public void testFindWithAsynchronousCustomExecutor() {
        long start = System.nanoTime();
        List<String> result = ShopService.findPricesASynchronousCustomExecutor("aPhone", SHOP_NUM);
        result.forEach(System.out::println);
        long end = System.nanoTime();
        System.out.println("Run took " + (end - start) / 1_000_000 + " m Sec");
    }
}
