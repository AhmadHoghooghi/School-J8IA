package util;

import shop.domain.Shop;
import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RunProfilerTest {


    private final Shop shop = new Shop("myFavoriteShop");

    @Test
    public void testSynchronizedGetPrice (){
        long took = RunProfiler.calculateRunTime(() -> shop.getPrice("myPhone"));
        System.out.println("took = " + took);
    }

    @Test
    public void testAsynchronousGetPrice() throws InterruptedException, ExecutionException, TimeoutException {
        long start = System.nanoTime();
        RunReport<CompletableFuture<Double>> runReport = RunProfiler.calculateRunTime(shop::getPriceAsync, "myPhone");
        System.out.println("invocation took = "+runReport.getTook());
        Double price = runReport.getRunResult().get(10, TimeUnit.SECONDS);
        long end = System.nanoTime();
        System.out.println("price is : "+ price);
        System.out.println("price retrial took "+ (end-start)/1_000_000);
    }

    @Test
    public void createCompletableFutureWithSupplayAsync() throws ExecutionException, InterruptedException {
        CompletableFuture<Double> futurePrice = CompletableFuture.supplyAsync(() -> shop.getPrice("yourPhone"));
        System.out.println(futurePrice.get());
    }
}
