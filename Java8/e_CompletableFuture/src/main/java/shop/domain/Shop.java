package shop.domain;

import discount.domain.DiscountCode;
import util.DelayUtil;

import java.util.Random;
import java.util.concurrent.CompletableFuture;

public class Shop {
    private static Random random = new Random();
    private String name;

    public Shop(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getPrice(String product) {
        return calculatePrice(product);
    }

    public String getPriceAndDiscount(String product) {
        DiscountCode[] discountsArray = DiscountCode.values();

        String resutlt = "";
        resutlt+=getName()+":";
        resutlt+=String.valueOf(calculatePrice(product))+":";
        resutlt+=discountsArray[new Random().nextInt(discountsArray.length)];
        return resutlt;
    }

    private double calculatePrice(String product) {
        DelayUtil.delayOSec();
        return random.nextDouble() * product.charAt(0) + product.charAt(1);
    }



    public CompletableFuture<Double> getPriceAsync(String product) {
        CompletableFuture<Double> priceF = new CompletableFuture<>();
        Thread thread = new Thread(() -> {
            double price = calculatePrice(product);
            try {
                priceF.complete(price);
            } catch (Exception ex){
                priceF.completeExceptionally(ex);
            }
        });
        thread.start();
        return priceF;
    }

}
