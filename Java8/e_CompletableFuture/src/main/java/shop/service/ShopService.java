package shop.service;

import util.ExecutorUtil;
import util.VendorList;

import java.util.concurrent.CompletableFuture;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import static java.util.stream.Collectors.toList;

public class ShopService {

    public static List<String> findPricesSynchronous(String product, int shopNum) {
        return VendorList.getShops(shopNum).stream().map(shop -> String.format("%s price is %.2f", shop.getName(), shop.getPrice(product))).collect(toList());
    }

    public static List<String> findPricesParallelStream(String product, int shopNum) {
         return VendorList.getShops(shopNum).parallelStream().map(shop -> String.format("%s price is %.2f", shop.getName(), shop.getPrice(product))).collect(toList());
    }

    public static List<String> findPricesASynchronousDefaultExecutor(String product, int shopNum) {
        List<CompletableFuture<String>> futureResultList = VendorList.getShops(shopNum).stream()
                .map(shop -> CompletableFuture.supplyAsync(() -> shop.getName() + " price is " + shop.getPrice(product)))

                .collect(toList());
        List<String> resultList = futureResultList.stream().map(CompletableFuture::join).collect(toList());
        return resultList;
    }

    public static List<String> findPricesASynchronousCustomExecutor(String product, int shopNum) {
        int threadNum = Math.min(VendorList.getShops(shopNum).subList(0, shopNum).size(), 100);
        Executor executor = ExecutorUtil.getExecutor(threadNum);
        List<CompletableFuture<String>> futureResultList = VendorList.getShops(shopNum).stream()
                .map(shop -> CompletableFuture.supplyAsync(() -> shop.getName() + " price is " + shop.getPrice(product), executor))

                .collect(toList());
        List<String> resultList = futureResultList.stream().map(CompletableFuture::join).collect(toList());
        return resultList;
    }


}
