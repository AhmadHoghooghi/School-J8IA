package util;

public class RunReport<T> {
    private long took;
    private T t;

    public RunReport(long took, T t) {
        this.took = took;
        this.t = t;
    }

    public long getTook() {
        return took;
    }

    public T getRunResult() {
        return t;
    }
}
