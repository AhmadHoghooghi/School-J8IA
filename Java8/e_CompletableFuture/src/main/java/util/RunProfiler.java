package util;

import java.util.function.Function;

public class RunProfiler {

    public static long calculateRunTime(Runnable runnable) {
        long start = System.nanoTime();
        runnable.run();
        long end = System.nanoTime();
        return (end-start)/1_000_000;
    }

    public static <T,R> RunReport<R> calculateRunTime(Function<T,R> function, T t){
        long start = System.nanoTime();
        R r = function.apply(t);
        long end = System.nanoTime();
        long dur = (end-start)/1_000_000;
        return  new RunReport<R>(dur,r);
    }


}

