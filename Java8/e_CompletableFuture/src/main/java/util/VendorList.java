package util;

import shop.domain.Shop;

import java.util.Arrays;
import java.util.List;

public class VendorList {
    private static List<Shop> shops = Arrays.asList(
            new Shop("Best Price"),
            new Shop("LestSaveBig"),
            new Shop("MyFavoriteShop"),
            new Shop("ByItAll"),
            new Shop("Gold Price"),
            new Shop("Etka"),
            new Shop("Taysiz"),
            new Shop("BestShop"),
            new Shop("YourShop"));

    public static List<Shop> getShops(int shopNum){
        return shops.subList(0, shopNum);
    }
}
