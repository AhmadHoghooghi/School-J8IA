package stream.chapter6;

import chapter6.Menu;
import chapter6.domain.Dish;
import org.junit.Test;

import static java.util.stream.Collectors.joining;

public class JoiningStringsDemo {
    @Test
    public void joinDishNames(){
        String DishNames = Menu.getDishs().stream().map(Dish::getName).collect(joining(","));
//        System.out.println(DishNames);
    }
}
