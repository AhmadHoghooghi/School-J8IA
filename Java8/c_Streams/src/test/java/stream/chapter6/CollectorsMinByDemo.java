package stream.chapter6;

import chapter6.Menu;
import chapter6.domain.Dish;
import org.junit.Test;

import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.reducing;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;

public class CollectorsMinByDemo {

    @Test
    public void findDishWithMinCalorie(){
        Menu.getDishs().stream()
                .collect(Collectors.minBy(comparing(Dish::getCalories)))
                .ifPresent(System.out::println);
    }



}
