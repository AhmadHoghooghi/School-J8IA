package stream.chapter6;

import chapter6.Menu;
import chapter6.domain.Dish;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.reducing;

public class ReduceVsCollectDemo {
    @Test
    public void reduceFirstForm() {
        Optional<Dish> DishWithFewerCalorie = Menu.getDishs().stream()
                .reduce((d1, d2) -> d1.getCalories() < d2.getCalories() ? d1 : d2);
    }

    @Test
    public void reduceSecondForm() {
        Dish ghormeSabzi = new Dish("GhormeSabzi", false, 750, Dish.Type.MEAT);
        Dish min = Menu.getDishs().stream()
                .reduce(ghormeSabzi, (d1, d2) -> d1.getCalories() < d2.getCalories() ? d1 : d2);
    }

    @Test
    public void reduceThirdForm() {
        int i = Stream.of("2", "3", "4", "5")
                .parallel()
                .reduce(0, (num, s) -> Integer.sum(num, Integer.parseInt(s)),
                        Integer::sum);
    }

    @Test
    public void CollectFirstForm() {
        Optional<Dish> oneDish = Menu.getDishs().stream().collect(reducing((d1, d2) -> d1.getCalories() < d2.getCalories() ? d1 : d2));
    }

    @Test
    public void CollectSecondtForm() {
        Dish ghormeSabzi = new Dish("GhormeSabzi", false, 750, Dish.Type.MEAT);
        Dish dish = Menu.getDishs()
                .stream().collect(reducing(ghormeSabzi, (d1, d2) -> d1.getCalories() < d2.getCalories() ? d1 : d2));
    }

    @Test
    public void CollectThirdForm() {
        List<String> list = Arrays.asList("Mukesh", "Vishal", "Amar");
        String result = list.parallelStream().collect(StringBuilder::new,
                (response, element) -> response.append(" ").append(element),
                (response1, response2) -> response1.append(",").append(response2.toString()))
                .toString();
        System.out.println("Result: " + result);
    }
}