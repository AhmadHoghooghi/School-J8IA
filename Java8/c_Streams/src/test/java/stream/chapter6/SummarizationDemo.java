package stream.chapter6;

import chapter6.Menu;
import chapter6.domain.Dish;
import org.junit.Test;


import java.util.IntSummaryStatistics;

import static java.util.stream.Collectors.*;

public class SummarizationDemo {

    @Test
    public void totalCaloriesInMenu(){
        Integer sumOfCaloriesInMenu = Menu.getDishs().stream().collect(summingInt(Dish::getCalories));
//        System.out.println(sumOfCaloriesInMenu);
    }

    @Test
    public void averageOfCaloriesInMenu(){
        Double average = Menu.getDishs().stream().collect(averagingInt(Dish::getCalories));
//        System.out.println(average);
    }

    @Test
    public void summaryStatistics(){
        IntSummaryStatistics summary = Menu.getDishs().stream().collect(summarizingInt(Dish::getCalories));
//        System.out.println(summary);
    }


}
