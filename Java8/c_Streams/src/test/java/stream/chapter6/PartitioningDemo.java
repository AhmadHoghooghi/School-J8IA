package stream.chapter6;

import chapter6.Menu;
import chapter6.domain.Dish;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.partitioningBy;

public class PartitioningDemo {

    @Test
    public void partitioningForVegetarian() {
        Map<Boolean, List<Dish>> partitionedMenuForVegetarian = Menu.getDishs()
                .stream().collect(partitioningBy(Dish::isVegetarian));
        System.out.println(partitionedMenuForVegetarian);
    }

    @Test
    public void partitioningForVegetarian_DishName() {
        Map<Boolean, List<Dish>> partitionedMenuForVegetarian = Menu.getDishs()
                .stream().collect(partitioningBy(Dish::isVegetarian));
        //Map<Boolean, List<String>>
    }

    @Test
    public void partitinNumbersToPrimeAndEven() {
        int MaxNum = 100;
        Map<Boolean, List<Integer>> collect = IntStream.rangeClosed(2, 100).boxed().collect(partitioningBy(i -> isPrime(i)));
        System.out.println(collect);
    }

    private boolean isPrime(int n) {
        int candidateRoot = (int) Math.sqrt(n);
        return IntStream.rangeClosed(2, candidateRoot).noneMatch(i -> n % i == 0);
    }
}
