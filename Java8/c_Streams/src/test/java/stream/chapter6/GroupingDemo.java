package stream.chapter6;

import chapter6.Menu;
import chapter6.domain.Dish;
import org.junit.Test;
import chapter6.domain.CaloricLevel;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;


public class GroupingDemo {


    @Test
    public void groupDishesByType() {
        //groupingBy(Function<? super T,? extends K> classifier)
        Map<Dish.Type, List<Dish>> dishesByType = Menu.getDishs().stream().collect(groupingBy(Dish::getType));
        dishesByType.keySet().stream().forEach(key -> {
            System.out.println(key);
            System.out.println(dishesByType.get(key).stream().map(Dish::getName).collect(joining(", ")));
            System.out.println();
        });
    }

    @Test
    public void groupByCaloricLevel() {
        //groupingBy(Function<? super T,? extends K> classifier)
        Map<CaloricLevel, List<Dish>> dishesByCaloricLevel = Menu.getDishs().stream().collect(groupingBy(
                Dish::getCaloricLevelFor
        ));

        dishesByCaloricLevel.keySet().stream().forEach(key -> {
            System.out.println(key);
            System.out.println(dishesByCaloricLevel.get(key).stream().map(Dish::getName).collect(joining(", ")));
            System.out.println();
        });
    }

    @Test
    public void groupByTypeAndCaloricLevel() {
        Map<Dish.Type, Map<CaloricLevel, List<Dish>>> dishesByTypeAndCalorie = Menu.getDishs().stream()
                .collect(groupingBy(Dish::getType,
                        groupingBy(Dish::getCaloricLevelFor)));
//        System.out.println(dishesByTypeAndCalorie);
    }

    @Test
    public void countDishesInEachType(){
        Map<Dish.Type, Long> typeToCountMap = Menu.getDishs().stream()
                .collect(groupingBy(Dish::getType, counting()));
//        System.out.println(typeToCountMap);
    }

    @Test
    public void mostCaloricDishForEachType(){
        Map<Dish.Type, String> typeToMaxCaloricInGroupMap = Menu.getDishs().stream()
                .collect(groupingBy(Dish::getType, collectingAndThen(collectingAndThen(maxBy(comparing(Dish::getCalories)), Optional::get), Dish::getName)));
        System.out.println(typeToMaxCaloricInGroupMap);
    }


}