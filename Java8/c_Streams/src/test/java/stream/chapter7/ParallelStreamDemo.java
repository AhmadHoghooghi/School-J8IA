package stream.chapter7;

import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.stream.LongStream;

public class ParallelStreamDemo {
    @Test
    public void commonStreamSum() {
        long to = 10_000_000L;
        Instant start = Instant.now();
        LongStream.iterate(1L, i -> i++).limit(to).reduce(0L, Long::sum);
        Instant end = Instant.now();
        System.out.println(Duration.between(start, end));
    }

    @Test
    public void parallelStreamSum() {
        long to = 10_000_000L;
        Instant start = Instant.now();
        LongStream.iterate(1L, i -> i++).limit(to).parallel().reduce(0L, Long::sum);
        Instant end = Instant.now();
        System.out.println(Duration.between(start, end));
    }

    @Test
    public void getAwailableProcessorsNum(){
        System.out.println(Runtime.getRuntime().availableProcessors());
    }
}
