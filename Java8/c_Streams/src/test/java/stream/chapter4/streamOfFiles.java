package stream.chapter4;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class streamOfFiles {
    @Test
    public void countDistinctWordsInFile() throws IOException {

        long distinctWordsNum;
        try (Stream<String> lines = Files.lines(Paths.get("src/main/resources/text.txt"))) {
            //I think tagging stream below as words makes code more readable.
            Stream<String> words = lines.flatMap(line -> Arrays.stream(line.split(" ")));
            distinctWordsNum = words.distinct().count();
        }
        assertThat(distinctWordsNum, is(equalTo(12L)));
    }



}
