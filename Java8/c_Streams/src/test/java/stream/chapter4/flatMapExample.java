package stream.chapter4;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class flatMapExample {
    //Quiz 2 on page 128
    @Test
    public void createPairOfNumFromTwoList() {
        List<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Integer> list2 = new ArrayList<>(Arrays.asList(3, 4));
        List<int[]> pairs = list1.stream().flatMap(
                i ->
                        list2.stream().map(j -> new int[]{i, j})

        )
                .collect(toList());
        pairs.stream().forEach(pair -> System.out.println(Arrays.toString(pair)));

    }

    //Quiz 3 on page 128
    @Test
    public void createPairOfNumFromTwoListFilthreOnesWithSumDividableByThree() {
        List<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Integer> list2 = new ArrayList<>(Arrays.asList(3, 4));
        List<int[]> pairs = list1.stream().flatMap(
                i ->
                        list2.stream().map(j -> new int[]{i, j})

        ).filter(pair -> (pair[0] + pair[1]) % 3 == 0)
                .collect(toList());
        pairs.stream().forEach(pair -> System.out.println(Arrays.toString(pair)));

    }


}
