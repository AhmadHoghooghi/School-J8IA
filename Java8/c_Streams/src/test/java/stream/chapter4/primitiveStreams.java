package stream.chapter4;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.*;

import static org.hamcrest.MatcherAssert.assertThat;

public class primitiveStreams {

    @Test
    public void testRange() {
        long intNum = IntStream.range(1, 10).count();
        assertThat(intNum, is(equalTo(9L)));
    }

    @Test
    public void testRangeClosed() {
        long intNum = IntStream.rangeClosed(1, 10).count();
        assertThat(intNum, is(equalTo(10L)));
    }

    @Test
    public void testPythagoreanTriples() {
        IntStream.rangeClosed(1, 100).boxed().flatMap(
                a -> IntStream.rangeClosed(a, 100)
                        .filter(b -> Math.sqrt(a * a + b * b) % 1 == 0)
                        .mapToObj(b -> new int[]{a, b, (int) Math.sqrt(a * a + b * b)})
        );
    }

    @Test
    public void testPythagoreanTriplesOldWay() {
        List<int[]> resultList = new ArrayList<>();
        //a^2+b^2=c^2
        for (int a = 1; a <= 100; a++) {
            for (int b = a; b <= 100;b++){
                double c =Math.sqrt(a*a+b*b);
                if(c%1==0){
                    resultList.add(new int[]{a,b,(int)c});
                }
            }
        }
    }

}
