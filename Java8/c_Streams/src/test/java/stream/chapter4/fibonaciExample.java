package stream.chapter4;

import org.junit.Test;

import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class fibonaciExample {
    @Test//Java 8 in action approach
    public void testCreateFibonachi() {
        Stream.iterate(new Pair(0, 1), pair -> new Pair(pair.getB(), pair.getAPlusB()))
                .map(pair -> pair.getA()).limit(10)
                .forEach(System.out::println);
    }

    @Test
    public void testCreateFibonachiWithFibonachiCustomIterator() {
        Stream.iterate(1, new FibonachiStatefulUnaryOperator()).limit(10)
                .forEach(System.out::println);
    }

    @Test
    public void testCreateFibonachiWithStatefulGenerator(){
        Stream.generate(new FibonachiGenerator()).limit(10).forEach(System.out::println);
    }


}
class FibonachiGenerator implements Supplier<Integer>{
    private int previousValue=0;
    private int currentValue=1;
    @Override
    public Integer get() {
        int result = previousValue;
        previousValue = currentValue;
        currentValue = previousValue+result;
        return result;
    }
}

class FibonachiStatefulUnaryOperator implements UnaryOperator<Integer> {
    int previousValue=0;

    @Override
    public Integer apply(Integer integer) {
        int result = integer + previousValue;
        previousValue = integer;
        return result;
    }
}

class Pair {
    private int a;
    private int b;

    public Pair(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getAPlusB() {
        return a + b;
    }
}
