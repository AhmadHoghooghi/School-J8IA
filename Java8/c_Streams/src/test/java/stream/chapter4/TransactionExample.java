package stream.chapter4;

import chapter5.domain.Trader;
import chapter5.domain.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class TransactionExample {
    private static List<Trader> traders;
    private static List<Transaction> transactions;

    @BeforeClass
    public static void setupDomainObjects() {
        //traders
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");
        traders = new ArrayList<>(Arrays.asList(raoul, mario, alan, brian));

        //transactions
        transactions = new ArrayList<>(Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)

        ));
    }

    @Test
    public void quiz1() {
        List<Transaction> transactionList = transactions.stream().filter(t -> t.getYear() == 2011).
                sorted(comparing(Transaction::getValue)).collect(toList());
//        System.out.println("Quiz1");

    }

    @Test
    public void quiz2() {
        List<String> tradersCityList = traders.stream().map(trader -> trader.getCity()).distinct().collect(toList());
        assertThat(tradersCityList.size(),is(equalTo(2)));
//        System.out.println("Quiz2");
//        System.out.println(tradersCityList);

    }

    @Test
    public void quiz3(){
        List<Trader> cambridgeTraders = traders.stream().filter(trader -> trader.getCity().equals("Cambridge"))
                .sorted(Comparator.comparing(Trader::getName)).collect(toList());
        assertThat(cambridgeTraders.size(), is(equalTo(3)));
//        cambridgeTraders.forEach(System.out::println);
    }

    @Test
    public void quiz4(){
        String names = traders.stream().map(Trader::getName)
                .sorted(Comparator.naturalOrder()).reduce("", (n1, n2) -> n1 +" "+ n2);
        System.out.println(names);
    }
    @Test
    public void quiz5(){
        boolean isAnyTraderInMilan = traders.stream().anyMatch(trader -> trader.getCity().equals("Milan"));
        System.out.println(isAnyTraderInMilan? "There is Trader in Milan": "There is not Trader in milan");
    }

    @Test
    public void quiz6(){
        transactions.stream().filter(tr -> tr.getTrader().getCity().equals("Cambridge"))
                .map(Transaction::getValue).forEach(System.out::println);
    }
    @Test
    public void quiz7(){
        transactions.stream().map(Transaction::getValue)
                .reduce(Integer::max).ifPresent(mOptional-> System.out.println(mOptional.longValue()));
    }

    @Test
    public void quiz8(){
        Optional<Transaction> minTrans = transactions.stream().min(comparing(Transaction::getValue));

    }
}
