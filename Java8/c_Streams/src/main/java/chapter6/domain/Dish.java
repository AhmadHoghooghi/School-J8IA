package chapter6.domain;

public class Dish {
    private final String name;
    private final boolean vegetarian;
    private final int calories;
    private final Type type;

    public Dish(String name, boolean vegetarian, int calories, Type type) {
        this.name = name;
        this.vegetarian = vegetarian;
        this.calories = calories;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public int getCalories() {
        return calories;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "name='" + name + '\'' +
                ", vegetarian=" + vegetarian +
                ", calories=" + calories +
                ", type=" + type +
                '}';
    }

    public static CaloricLevel getCaloricLevelFor(Dish dish) {
        int calories = dish.getCalories();
        if (calories <= 400) {
            return CaloricLevel.Diet;
        } else if (calories <= 700) {
            return CaloricLevel.Normal;
        } else {
            return CaloricLevel.Fat;
        }
    }

    public enum Type { MEAT, FISH, OTHER }
}
