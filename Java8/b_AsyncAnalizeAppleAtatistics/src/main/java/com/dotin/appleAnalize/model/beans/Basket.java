package com.dotin.appleAnalize.model.beans;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Basket {
    @XmlElementWrapper(name = "appleBasket")
    @XmlElement(name = "apple")
    private List<Apple> appleList;

    public Basket() {
        appleList = new ArrayList<>();
    }

    public Basket(List<Apple> appleList) {
        setApples(appleList);
    }

    public List<Apple> getApples() {
        return appleList;
    }

    public void setApples(List<Apple> appleList) {
        if (appleList == null) {
            throw new NullPointerException("list can not be NULL");
        }
        this.appleList = appleList;
    }

    @Override
    public String toString() {
        return "Basket{" +
                "appleList=" + appleList +
                '}';
    }
}
