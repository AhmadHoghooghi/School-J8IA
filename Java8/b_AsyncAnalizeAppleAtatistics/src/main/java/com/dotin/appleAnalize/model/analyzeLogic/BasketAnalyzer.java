package com.dotin.appleAnalize.model.analyzeLogic;

import com.dotin.appleAnalize.data.PersistenceUtil;
import com.dotin.appleAnalize.model.beans.Apple;
import com.dotin.appleAnalize.model.beans.Basket;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class BasketAnalyzer {
    public static Map<String, Integer> analyzeBasket(Basket basket) {
        Map<String, Long> collect = basket.getApples().stream().collect(Collectors.groupingBy(Apple::getColor, counting()));
        return collect.keySet().stream().collect(toMap(Function.identity(), key -> (collect.get(key)).intValue()));
    }

    public static Map<String, Integer> analyzeBasketsAsyncronousInDirectory(File directory) {
        List<CompletableFuture<Map<String, Integer>>> statisticMapList = Arrays.stream(directory.listFiles())
                .map(BasketAnalyzer::xmlFileToCompletableFutureBasketFunction)
                .map(fut -> fut.thenCompose(
                        basket -> CompletableFuture.supplyAsync(() -> BasketAnalyzer.analyzeBasket(basket))
                )).collect(toList());

        List<Map<String, Integer>> completedStatistics = statisticMapList.stream()
                .map(CompletableFuture::join).collect(toList());
        Map<String, Integer> resultMap = new HashMap<>();
        for (Map<String, Integer> map : completedStatistics) {
            map.forEach((color, num) -> resultMap.merge(color, num, Integer::sum));
        }
        return resultMap;
    }

    private static CompletableFuture<Basket> xmlFileToCompletableFutureBasketFunction(File file) {
        CompletableFuture<Basket> basketFuture = new CompletableFuture<>();
        new Thread(() -> {
            try {
                Basket basket = PersistenceUtil.unMarshalBasketFromFile(file);
                basketFuture.complete(basket);
            } catch (JAXBException e) {
                basketFuture.completeExceptionally(e);
            }
        }).start();
        return basketFuture;
    }
}
