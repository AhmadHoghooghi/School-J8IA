package com.dotin.appleAnalize.data;

import com.dotin.appleAnalize.model.beans.Apple;
import com.dotin.appleAnalize.model.beans.Basket;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class PersistenceUtil {
    public static void marshalAppleToFile(Apple apple, File targetFile) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Apple.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(apple, targetFile);
    }

    public static Apple unMarshalAppleFromFile(File sourceFile) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Apple.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (Apple) unmarshaller.unmarshal(sourceFile);
    }

    public static void marshalBasketToFile(Basket basket, File targetFile) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Basket.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(basket,targetFile);
    }

    public static Basket unMarshalBasketFromFile(File sourceFile) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(Basket.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (Basket) unmarshaller.unmarshal(sourceFile);
    }
}
