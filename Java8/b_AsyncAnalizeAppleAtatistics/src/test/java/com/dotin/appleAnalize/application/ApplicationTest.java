package com.dotin.appleAnalize.application;

import com.dotin.appleAnalize.model.analyzeLogic.BasketAnalyzer;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;


public class ApplicationTest {
    @Test
    public void testAnalyzingFilesInResourcesAsyncMode() {
        File directory = new File("src/main/resources");
        Map<String, Integer> resultMap = BasketAnalyzer.analyzeBasketsAsyncronousInDirectory(directory);
        assertThat(resultMap, hasEntry("orange",23));
        assertThat(resultMap, hasEntry("red",18));
        assertThat(resultMap, hasEntry("blue",23));
        assertThat(resultMap, hasEntry("yellow",18));
    }


}
