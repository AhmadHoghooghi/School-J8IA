package com.dotin.appleAnalize.data;

import com.dotin.appleAnalize.model.beans.Apple;
import com.dotin.appleAnalize.model.beans.Basket;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GenerateFiles {
    private final List<String> colorList = new ArrayList<>(Arrays.asList("yellow", "red", "blue", "orange"));
    private final Random random = new Random();
    @Ignore
    @Test
    public void generateDataFilesToAnalyze() throws JAXBException {
        int FILE_NUM = 10;
        for (int i = 0; i < FILE_NUM; i++) {
            int MAX_APPLE_NUM_IN_BASKET = 15;
            int appleNumInBasket = random.nextInt(MAX_APPLE_NUM_IN_BASKET);
            //generate Apple List
            //generate basket Object
            Basket basket = new Basket(generateAppleList(appleNumInBasket));
            //write basket to file
            String filePath = "src/main/resources/file" + ((i < 10) ? "0" : "") +i+ ".xml";
            PersistenceUtil.marshalBasketToFile(basket, new File(filePath));
        }

    }

    private List<Apple> generateAppleList(int appleNumInBasket) {
        List<Apple> resultList = new ArrayList<>();
        for (int i = 0; i < appleNumInBasket; i++) {
            resultList.add(new Apple(randomColor(), randomWeight()));
        }
        return resultList;
    }

    private int randomWeight() {
        return random.nextInt(50) + 100;
    }

    private String randomColor() {
        int randomIndex = random.nextInt(4);
        return colorList.get(randomIndex);
    }
}
