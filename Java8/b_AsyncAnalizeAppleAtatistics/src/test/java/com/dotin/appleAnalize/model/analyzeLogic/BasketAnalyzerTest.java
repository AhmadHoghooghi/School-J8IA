package com.dotin.appleAnalize.model.analyzeLogic;

import com.dotin.appleAnalize.model.beans.Basket;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class BasketAnalyzerTest {

    @Test
    public void analyzeEmptyBasket() {
        Basket basket = new Basket(new ArrayList<>());
        Map<String, Integer> emptyBasketStatistics = BasketAnalyzer.analyzeBasket(basket);
        assertThat(emptyBasketStatistics.size(), is(0));
    }

    /*@Test
    public void analyzeEmptyBasketList() {
        List<Basket> basketList = new ArrayList<>();
        Map<String, Integer> basketStatistics = BasketAnalyzer.analyzeBaskets(basketList);
        assertThat(basketStatistics.size(), is(0));
    }*/
}
