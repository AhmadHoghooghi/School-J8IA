package com.dotin.appleAnalize.model.beans;

import com.dotin.appleAnalize.model.beans.Basket;
import org.junit.Test;

public class BasketTest {

    @Test(expected = NullPointerException.class)
    public void basketInstantiationWithNullList(){
        Basket basket = new Basket(null);
    }

    @Test(expected = NullPointerException.class)
    public void basketInstantiationWithNullListSet(){
        Basket basket = new Basket();
        basket.setApples(null);
    }
}
