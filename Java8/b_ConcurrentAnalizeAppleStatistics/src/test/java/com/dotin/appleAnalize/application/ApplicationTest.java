package com.dotin.appleAnalize.application;

import com.dotin.appleAnalize.data.PersistenceUtil;
import com.dotin.appleAnalize.model.analyzeLogic.BasketAnalyzer;
import com.dotin.appleAnalize.model.beans.Basket;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class ApplicationTest {

    @Test
    public void testAnalyzingFilesInResources() throws JAXBException, ExecutionException, InterruptedException {
        File directory = new File("src/main/resources");
        List<Basket> baskets = new ArrayList<>();
        for (File file:directory.listFiles()) {
            Basket basket = PersistenceUtil.unMarshalBasketFromFile(file);
            baskets.add(basket);
        }
        Map<String, Integer> statistics = BasketAnalyzer.analyzeBasketsInParallel(baskets);
        assertThat(statistics.size(), is(equalTo(4)));
        System.out.println(statistics);
    }
}
