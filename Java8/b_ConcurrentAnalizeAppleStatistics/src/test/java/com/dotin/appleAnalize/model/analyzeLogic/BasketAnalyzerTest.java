package com.dotin.appleAnalize.model.analyzeLogic;

import com.dotin.appleAnalize.model.beans.Apple;
import com.dotin.appleAnalize.model.beans.Basket;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.fail;


public class BasketAnalyzerTest {

    @Test
    public void analyzeEmptyBasket() {
        Basket basket = new Basket(new ArrayList<>());
        Map<String, Integer> emptyBasketStatistics = BasketAnalyzer.analyzeBasket(basket);
        assertThat(emptyBasketStatistics.size(), is(0));

    }

    @Test
    public void analyzeBasketWithOneTypeOfApple() {
        Apple redApple = new Apple("red", 130);
        Basket basket = new Basket(Collections.singletonList(redApple));
        Map<String, Integer> basketStatistics = BasketAnalyzer.analyzeBasket(basket);
        assertThat(basketStatistics.size(), is(1));
        assertThat(basketStatistics, hasEntry("red", 1));
//        System.out.println(basketStatistics);
    }

    @Test
    public void analyzeEmptyBasketList() {
        List<Basket> basketList = new ArrayList<>();
        Map<String, Integer> basketStatistics = BasketAnalyzer.analyzeBaskets(basketList);
        assertThat(basketStatistics.size(), is(0));
    }

    @Test
    public void analyzeBasketList(){
        //create basket 1
        Apple redApple1 = new Apple("red", 150);
        Basket basket1 = new Basket(Arrays.asList(redApple1));
        //create basket 2
        Apple yellowApple = new Apple("yellow" , 165);
        Apple redApple2 = new Apple("red", 190);
        Basket basket2 = new Basket(Arrays.asList(yellowApple, redApple2));
        //create basketList
        List<Basket> baskets = new ArrayList<>(Arrays.asList(basket1,basket2));
        //analyze basketList
        Map<String, Integer> accumulatedStatistics = BasketAnalyzer.analyzeBaskets(baskets);
        //assertResult
        assertThat(accumulatedStatistics.size(), is(2));
        assertThat(accumulatedStatistics, hasEntry("red", 2));
        assertThat(accumulatedStatistics, hasEntry("yellow", 1));
        //System.out.println(accumulatedStatistics);
    }

    @Test
    public void analyzeEmptyBasketListInParallel() throws ExecutionException, InterruptedException {
        List<Basket> basketList = new ArrayList<>();
        Map<String, Integer> basketStatistics = BasketAnalyzer.analyzeBasketsInParallel(basketList);
        assertThat(basketStatistics.size(), is(0));
    }

    @Test
    public void analyzeBasketListInParallel() throws ExecutionException, InterruptedException {
        //create basket 1
        Apple redApple1 = new Apple("red", 150);
        Basket basket1 = new Basket(Arrays.asList(redApple1));
        //create basket 2
        Apple yellowApple = new Apple("yellow" , 165);
        Apple redApple2 = new Apple("red", 190);
        Basket basket2 = new Basket(Arrays.asList(yellowApple, redApple2));
        //create basketList
        List<Basket> baskets = new ArrayList<>(Arrays.asList(basket1,basket2));
        //analyze basketList
        Map<String, Integer> accumulatedStatistics = BasketAnalyzer.analyzeBasketsInParallel(baskets);
        //assertResult
        assertThat(accumulatedStatistics.size(), is(2));
        assertThat(accumulatedStatistics, hasEntry("red", 2));
        assertThat(accumulatedStatistics, hasEntry("yellow", 1));
        //System.out.println(accumulatedStatistics);
    }
}
