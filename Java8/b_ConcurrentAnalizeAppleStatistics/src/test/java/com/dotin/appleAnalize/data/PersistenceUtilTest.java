package com.dotin.appleAnalize.data;

import com.dotin.appleAnalize.model.beans.Apple;
import com.dotin.appleAnalize.model.beans.Basket;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.fail;


public class PersistenceUtilTest {
    @Test
    public void testAppleMarshalToFile() throws JAXBException {
        Apple apple = new Apple("blue", 110);
        File file = new File("apple-marshal.xml");
        PersistenceUtil.marshalAppleToFile(apple,file);
        file.delete();
    }

    @Test
    public void testUnmarshalAppleFromFile() throws JAXBException {
        Apple apple = new Apple("blue", 110);
        File file = new File("apple-unMarshal.xml");
        PersistenceUtil.marshalAppleToFile(apple,file);
        Apple retrievedApple = PersistenceUtil.unMarshalAppleFromFile(file);
        assertThat(retrievedApple,is(notNullValue()));
        assertThat(retrievedApple.getColor(), is(equalTo("blue")));
        assertThat(retrievedApple.getWeight(), is(equalTo(110)));
        file.delete();
    }

    @Test
    public void marshalBasketHappyPath() throws JAXBException {
        Apple apple1 = new Apple("black", 120);
        Apple apple2 = new Apple("blue", 130);
        Apple apple3 = new Apple("red", 140);

        List<Apple> appleList = Arrays.asList(apple1,apple2,apple3);
        Basket basket = new Basket(appleList);

        File file = new File("basket-marshal.xml");
        PersistenceUtil.marshalBasketToFile(basket,file);
        file.delete();

    }
    @Test
    public void unMarshalBasketHappyPath() throws JAXBException {
        Apple apple1 = new Apple("black", 120);
        Apple apple2 = new Apple("blue", 130);
        Apple apple3 = new Apple("red", 140);

        List<Apple> appleList = Arrays.asList(apple1,apple2,apple3);
        Basket basket = new Basket(appleList);

        File file = new File("basket-unMarshal.xml");
        PersistenceUtil.marshalBasketToFile(basket,file);
        Basket retrievedBasket = PersistenceUtil.unMarshalBasketFromFile(file);
        assertThat(retrievedBasket, is(notNullValue()));
        assertThat(3, is(equalTo(retrievedBasket.getApples().size())));
        file.delete();

    }

    @Test
    public void marshalEmptyBasket() throws JAXBException {
        Basket basket = new Basket();

        File file = new File("empty-basket.xml");
        PersistenceUtil.marshalBasketToFile(basket,file);
        file.delete();
    }

    @Test
    public void unMarshalEmptyBasket() throws JAXBException {
        Basket basket = new Basket();

        File file = new File("empty-basket-unMarshal.xml");
        PersistenceUtil.marshalBasketToFile(basket,file);
        Basket retrievedBasket = PersistenceUtil.unMarshalBasketFromFile(file);
        assertThat(retrievedBasket, is(notNullValue()));
        assertThat(0, is(equalTo(retrievedBasket.getApples().size())));
        //System.out.println(retrievedBasket);
        file.delete();
    }

    @Test(expected = JAXBException.class)

    public void unMarshalBasketFromEmptyFile() throws JAXBException, IOException {
        File file = new File("emptyFile.xml");
        file.createNewFile();
        try {
            Basket basket = PersistenceUtil.unMarshalBasketFromFile(file);
        }catch (JAXBException e){
            throw e;
        }finally {
            file.delete();
        }
    }

}
