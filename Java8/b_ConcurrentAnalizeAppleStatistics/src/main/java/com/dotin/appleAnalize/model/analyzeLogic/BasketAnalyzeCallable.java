package com.dotin.appleAnalize.model.analyzeLogic;

import com.dotin.appleAnalize.model.beans.Basket;

import java.util.Map;
import java.util.concurrent.Callable;

public class BasketAnalyzeCallable implements Callable <Map<String, Integer>> {
    private Basket basket;

    public BasketAnalyzeCallable(Basket basket) {
        this.basket = basket;
    }


    @Override
    public Map<String, Integer> call() throws Exception {
        return BasketAnalyzer.analyzeBasket(basket);
    }
}
