package com.dotin.appleAnalize.model.analyzeLogic;

import com.dotin.appleAnalize.model.beans.Apple;
import com.dotin.appleAnalize.model.beans.Basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class BasketAnalyzer {
    public static Map<String, Integer> analyzeBasket(Basket basket) {
        Map<String, Integer> resultMap = new HashMap<>();
        for (Apple apple : basket.getApples()) {
            Integer numOfApplesWithThisColor = resultMap.get(apple.getColor());
            if (numOfApplesWithThisColor == null) {
                numOfApplesWithThisColor = 0;
            }
            resultMap.put(apple.getColor(), numOfApplesWithThisColor + 1);
        }
        return resultMap;
    }

    public static Map<String, Integer> analyzeBaskets(List<Basket> basketList) {
        Map<String, Integer> resultMap = new HashMap<>();
        //Since Java 8 Map contains merge method which requires
        //key, new value, and function which will be used to decide
        // what value to put in map if it already contains our key
        for (Basket basket : basketList) {
            Map<String , Integer> basketStatistics = analyzeBasket(basket);
            basketStatistics.forEach((color, num)-> resultMap.merge(color , num, Integer::sum));
        }
        return resultMap;
    }

    /**
     * This method crates a FixedThreadPool with thread number equal to basket list size.
     * @param basketList
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static Map<String, Integer> analyzeBasketsInParallel(List<Basket> basketList) throws ExecutionException, InterruptedException {
        Map<String, Integer> resultMap = new HashMap<>();
        if (basketList.size()==0){
            return resultMap;
        }
        ExecutorService executorService = Executors.newFixedThreadPool(basketList.size());

        List<Future<Map<String,Integer>>> futureObjectsList = new ArrayList<>();
        //start analyzing baskets in parallel
        for (Basket basket : basketList) {
            Future<Map<String,Integer>> basketStatisticsFuture = executorService.submit(new BasketAnalyzeCallable(basket));
            futureObjectsList.add(basketStatisticsFuture);
        }
        //accumulate statistics
        for (Future<Map<String, Integer>> statisticsFuture:futureObjectsList){
            Map<String , Integer> basketStatistics = statisticsFuture.get();
            basketStatistics.forEach((color, num)-> resultMap.merge(color , num, Integer::sum));
        }
        return resultMap;
    }
}
