package optional.duration;

import java.util.Optional;
import java.util.Properties;

public class DurationReader {
    static Properties properties = new Properties();

    static {
        properties.setProperty("5", "5");
        properties.setProperty("true", "true");
        properties.setProperty("-3", "-3");
    }

    public static int readDuration(String name) {
        String propertyValue = (String) properties.get(name);
        return Optional.ofNullable(propertyValue).flatMap(DurationReader::stringToOptionalInteger)
                .filter(i -> i >= 0).orElse(0);
    }

    private static Optional<Integer> stringToOptionalInteger(String numString) {
        try {
            return Optional.of((Integer.parseInt(numString)));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }
}
