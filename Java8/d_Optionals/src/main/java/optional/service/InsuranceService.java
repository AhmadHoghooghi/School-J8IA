package optional.service;

import optional.domain.Car;
import optional.domain.Insurance;
import optional.domain.Person;

import java.util.Optional;

public class InsuranceService {
    public static Optional<Insurance> nullSafeFindCheapestInsurance(Optional<Person> personOp, Optional<Car> carOp ){
        return personOp.flatMap(p -> carOp.map(c -> notNullSafeFindCheapestInsurance(p, c)));
    }

    private static Insurance notNullSafeFindCheapestInsurance(Person person, Car car){
        if (person == null) {
            throw new IllegalArgumentException("person can not be null");
        }
        if (car == null) {
            throw new IllegalArgumentException("car can not be null");
        }

        return new Insurance("pasargad");
    }

}
