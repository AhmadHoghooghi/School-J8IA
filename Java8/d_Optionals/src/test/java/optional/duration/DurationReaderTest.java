package optional.duration;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class DurationReaderTest {
    @Test
    public void testReadDurationHappyPath() {
        int a = DurationReader.readDuration("5");
        assertThat(a, is(equalTo(5)));
    }

    @Test
    public void testReadDurationFromUnparsableDuration() {
        int a = DurationReader.readDuration("true");
        assertThat(a, is(equalTo(0)));
    }

    @Test
    public void testReadDurationFromNegativeValue() {
        int a = DurationReader.readDuration("-3");
        assertThat(a, is(equalTo(0)));
    }

    @Test
    public void testReadDurationFromNullValue() {
        int a = DurationReader.readDuration("Unknown");
        assertThat(a, is(equalTo(0)));
    }
}
