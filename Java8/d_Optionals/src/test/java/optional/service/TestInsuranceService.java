package optional.service;

import optional.domain.Car;
import optional.domain.Insurance;

import optional.domain.Person;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;

import static org.hamcrest.MatcherAssert.assertThat;

public class TestInsuranceService {

    @Test
    public void findInsuranceWithEmptyPerson() {
        Car car = new Car(Optional.empty());
        Optional<Insurance> insurance = InsuranceService.nullSafeFindCheapestInsurance(Optional.empty(), Optional.of(car));
        assertThat(insurance.isPresent(), is(equalTo(false)));
    }

    @Test
    public void findInsuranceWithEmptyCar() {
        Person person = new Person(Optional.empty());
        Optional<Insurance> insurance = InsuranceService.nullSafeFindCheapestInsurance(Optional.of(person), Optional.empty());
        assertThat(insurance.isPresent(), is(equalTo(false)));
    }

    @Test
    public void findInsuranceWithEmptyParameters() {
        Car car = new Car(Optional.empty());
        Person person = new Person(Optional.of(car));
        Optional<Insurance> insurance = InsuranceService.nullSafeFindCheapestInsurance(Optional.of(person), Optional.of(car));
       // assertions
        assertThat(insurance.isPresent(), is(equalTo(true)));
        assertThat(insurance.get().getName(), is(equalTo("pasargad")));
    }
}
